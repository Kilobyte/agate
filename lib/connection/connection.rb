require 'socket'
require 'command/command_manager'
require 'command/module'

module Agate
  module Connection
    class Connection
      attr_accessor :user, :server, :nick

      def initialize(socket)
        @user = @server = nil
        @nick = '*'
        @socket = socket
        t = Thread.new do
          loop do
            begin
              break if readln
            rescue Exception => e
              puts "Uncaught Exception in Listen Thread: #{e.message}"
              puts e.backtrace.join "\n"
            end
          end
        end
        t.abort_on_exception = true
      end

      def send_line(line_info)
        prefix = line_info[:prefix]
        cmd = line_info[:command]
        params = line_info[:params]
        data = nil
        if params.last.include? ' '
          data = params.pop
        end
        params = params.join(' ')
        params = params[1..params.length] if params[0] == ''
        line = "#{prefix.nil? ? '' : ":#{prefix} "}#{cmd}#{params.length > 0 ? " #{params}" : ''}#{data.nil? ? '' : " :#{data}"}"
        @socket.puts line
        puts "<<< #{line}"
      end

      def close
        @socket.close
      end

      def hostname
        begin
          port, ip = Socket.unpack_sockaddr_in(@socket.getpeername)
          Socket.gethostbyaddr(ip)
        rescue
          ip
        end
      end

      private
      def readln
        # listen loop
        line = @socket.gets
        if line.nil? || line == ''
          return true
        end
        puts ">>> #{line}"
        line_info = Agate::Command.parse_line line, @user || @server
        line_info[:con] = self
        unless (user && user.modes[:u]) || !@server.nil?
          line_info[:origin] = @user
        end
        Agate::Command::Manager.call_command line_info
        false
      end

    end
  end
end