require 'irc/user'
#require 'connection/server'
require 'irc/replies'

module Agate
  module Connection

    def self.resolve_origin(origin)
      User.find(origin)
    end

    def self.resolve_target(target)
      if target[0] == '#'
        nil
      else
        Agate::IRC::User.find(target)
      end
    end

    def self.introduce(con)
      n = con.nick
      [
          [IRC::RPL.WELCOME, [n, 'Welcome to TestNet']],
          [IRC::RPL.YOURHOST, [n, 'Your host is Agate']],
          [IRC::RPL.CREATED, [n, 'This server was created somewhen in the past']],
          [IRC::RPL.MYINFO, [n, 'agate', '0.0.1', 'oui', 'ov']],
          [IRC::RPL.ERR_NOMOTD, [n, 'MOTD file is missing']]
      ].each do |rpl|
        con.send_line({:prefix => 'test.server.kilobyte22.de', :params =>  rpl[1], :command => rpl[0]})
      end
    end
  end
end