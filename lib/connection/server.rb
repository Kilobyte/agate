require 'socket'
require 'connection/connection'

module Agate
  module Connection
    class Server
      def initialize(port)

        @port = port
      end

      def start
        @server = TCPServer.new(@port)
        loop {
          con = Agate::Connection::Connection.new @server.accept
          puts 'DEBUG: Incoming connection'
        }
      end

      def stop

      end
    end
  end
end