module Agate
  module IRC
    module RPL
      @@replies = {
          WELCOME: '001',
          YOURHOST: '002',
          CREATED: '003',
          MYINFO: '004',

          ERR_NOMOTD: '422'
      }
      def self.method_missing(name, *args, &block)
        rpl = @@replies[name]
        if rpl
          rpl
        else
          super
        end
      end
    end
  end
end