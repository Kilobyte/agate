module Agate
  module IRC
    class User

      @@users = {}
      def self.find(nick)
        @@users[nick.downcase]
      end

      attr_reader :nick, :ident, :host, :connection, :realname, :hops, :modes

      def initialize(nick, ident, host, realname, hops, connection)
        @modes = {}
        @nick = nick
        @ident = ident
        @host = host
        @connection = connection
        @hops = hops
        @realname = realname
        @@users[nick.downcase] = self
      end

      def local?
        true
      end

      def trusted?
        !@modes[:m].nil?
      end

      def hostmask
        "#{nick}!#{ident}@#{host}"
      end

      def nick=(nick)
        @@users[@nick.downcase] = nil
        @nick = nick
        @@users[nick.downcase] = self
      end

      def send_line(line_info)
        @connection.send_line line_info
      end

    end
  end
end