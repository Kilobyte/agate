require 'module/module_system'
module Agate
  module Command
    module Manager

      @commands = {}

      def self.register_command(mod, name, &block)
        name = name.to_s.downcase
        return false unless @commands[name].nil?
        @commands[name] = {:module => mod, :name => name, :cb => block}
        true
      end

      def self.unregister_command(name)
        @commands[name.to_s.downcase] = nil
      end

      def self.call_command(message_details)
        c = @commands[message_details[:command].to_s.downcase]
        if c.nil?
          false
        else
          if Agate::ModuleSystem.pre_call c[:module]
            c[:cb].call message_details
            Agate::ModuleSystem.post_call
          end
        end
      end
    end
  end
end