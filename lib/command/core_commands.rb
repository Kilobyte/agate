require 'connection/module'
require 'command/command_manager'

module Agate
  module Command
    module Core
      def self.command(name, &block)
        Agate::Command::Manager.register_command :core, name, &block
      end

      command :nick do |cmd|
        con = cmd[:con]
        if cmd[:params].length < 1
          #con.send_reply(Irc::RPL)
        end

        origin = cmd[:origin]
        if origin.nil?
          con.nick = cmd[:params][0]
        else
          origin.nick = cmd[:params]
        end
      end

      command :user do |cmd|
        con = cmd[:con]
        origin = cmd[:origin]
        trusted = (!origin.nil?) && origin.trusted?

        p = cmd[:params]
        ident = p[0]
        hops = trusted ? p[1] : 0
        realname = p[3]
        con.user = Agate::IRC::User.new con.nick, "~#{ident}", con.hostname, realname, hops, trusted ? nil : con
        Agate::Connection.introduce(con)
      end

      command :privmsg do |cmd|
        targets = cmd[:params][0]
        msg = cmd[:params][1]
        targets.split(',').keep_if{|t| t.length > 0}.each do |t|
          tar = Agate::Connection.resolve_target t
          line = {:prefix => cmd[:origin].hostmask, :command => 'PRIVMSG', :params => [t, msg]}
          tar.send_line line
        end
      end

      command :quit do |cmd|
        msg = cmd[:params][0]
        con = cmd[:con]
        origin = cmd[:origin]
        if origin.local?
          con.send_line({:command => 'ERROR', :params => ['Connection closed']})
          con.close
        end
      end

    end
  end
end