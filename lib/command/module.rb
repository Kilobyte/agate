require 'connection/module'

module Agate
  module Command
    def self.parse_line(line, origin)
      line =~ /^(:[^ ]+ )?([^ ]+)(.*?)(?: :(.*))?$/
      line_info = {:prefix => $1, :command => $2, :params => $3 ? $3.split(' ') : []}
      line_info[:params] << $4 unless $4.nil?
      if $1.nil?
        line_info[:origin] = origin
      else
        line_info[:origin] = Agate::Connection.resolve $1
      end
      line_info
    end
  end
end