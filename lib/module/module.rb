require 'command/command_manager'

module Agate
  class AgateModule

    @@props = [:name, :author, :version]

    @@props.each do |prop|
      define_method prop do |arg|
        instance_variable_set prop.to_s, arg
      end
    end
    @commands = {}

    def irc_command(name, &block)
      name = name.to_s.downcase
      @commands[name] = {:cb => block} if Agate::Command::Manager.register_command @name, name, block
    end

    def pre_unload
      unload
      @commands.each_key do |cmd|
        Agate::Command::Manager.unregister_command cmd
      end
    end

    def unload; end
  end
end
