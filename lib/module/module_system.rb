module Agate
  module ModuleSystem
    @modules = {}
    def self.module_call(mod, call, *args)
      if pre_call(mod)
        @modules[mod][:class].send(call, *args)
        post_call
      end
    end

    # call this whenever you do a call to a plugin without use of module_call.
    # not doing so WILL screw up stuff. if this returns false, abort the call
    def self.pre_call(mod)
      ensure_thread
      modstack << mod.to_sym
    end

    # do this after a call to a plugin. again, module_call does this itself.
    # if the connected pre_call returned false you MUST NOT call this.
    def self.post_call
      ensure_thread
      self.modstack.pop
    end

    def self.effective_module
      self.modstack.peek
    end

    private
    def self.ensure_thread
      Thread.current[:module_stack] ||= []
    end

    def self.modstack
      Thread.current[:module_stack]
    end
  end
end