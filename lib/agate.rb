require 'command/command_manager'
require 'command/core_commands'

require 'connection/server'

server = Agate::Connection::Server.new(8000)
server.start

trap :INT do
    server.stop
end